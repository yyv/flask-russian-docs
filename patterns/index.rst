.. _patterns:

Заготовки для Flask
===================

Некоторые вещи являются настолько общими, что существует большой шанс найти
их в большинстве веб-приложений.  К примеру, довольно много приложений
используют реляционные базы данных и аутентификацию пользователей.  В этом
случае, скорее всего, ими в начале запроса будут открываться подключения к
базе данных и получаться информация о пользователе, вошедшем в систему.
В конце запроса, соединение с базой данных вновь закрывается.

В `Архиве фрагментов для Flask <http://flask.pocoo.org/snippets/>`_ можно
найти много поддерживаемых энтузиастами фрагментов и заготовок.

.. toctree::
   :maxdepth: 2

   packages
   appfactories
   appdispatch
   apierrors
   urlprocessors
   distribute
   fabric
   sqlite3
   sqlalchemy
   fileuploads
   caching
   viewdecorators
   wtforms
   templateinheritance
   flashing
   jquery
   errorpages
   lazyloading
   mongokit
   favicon
   streaming
   deferredcallbacks
   methodoverrides
   requestchecksum

`Оригинал этой страницы <http://flask.pocoo.org/docs/patterns/>`_