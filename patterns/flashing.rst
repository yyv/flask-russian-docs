.. _message-flashing-pattern:

Всплывающие сообщения 
=====================

Хорошие приложения и интерфейсы пользователя дают обратную связь.  Если
пользователь не получает достаточной обратной связи, вскоре он может начать
ненавидеть приложение.  Flask предоставляет по-настоящему простой способ
дать обратную связь пользователю при помощи системы всплывающих сообщений.
Система всплывающих сообщений обычно делает возможным записать сообщение в
конце запроса и получить к нему доступ во время обработки следующего и
только следующего запроса.  Обычно эти сообщения используются в шаблонах
макетов страниц, которые его и отображают.

Пример всплывающих сообщений
----------------------------

Вот полный пример::

    from flask import Flask, flash, redirect, render_template, \
         request, url_for

    app = Flask(__name__)
    app.secret_key = 'some_secret'

    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/login', methods=['GET', 'POST'])
    def login():
        error = None
        if request.method == 'POST':
            if request.form['username'] != 'admin' or \
                    request.form['password'] != 'secret':
                error = 'Invalid credentials'
            else:
                flash('You were successfully logged in')
                return redirect(url_for('index'))
        return render_template('login.html', error=error)

    if __name__ == "__main__":
        app.run()


А вот шаблон макета ``layout.html``, отображающий сообщение:

.. sourcecode:: html+jinja

   <!doctype html>
   <title>My Application</title>
   {% with messages = get_flashed_messages() %}
     {% if messages %}
       <ul class=flashes>
       {% for message in messages %}
         <li>{{ message }}</li>
       {% endfor %}
       </ul>
     {% endif %}
   {% endwith %}
   {% block body %}{% endblock %}

А это - шаблон index.html:

.. sourcecode:: html+jinja

   {% extends "layout.html" %}
   {% block body %}
     <h1>Overview</h1>
     <p>Do you want to <a href="{{ url_for('login') }}">log in?</a>
   {% endblock %}

И, конечно, шаблон страницы входа:

.. sourcecode:: html+jinja

   {% extends "layout.html" %}
   {% block body %}
     <h1>Login</h1>
     {% if error %}
       <p class=error><strong>Error:</strong> {{ error }}
     {% endif %}
     <form action="" method=post>
       <dl>
         <dt>Username:
         <dd><input type=text name=username value="{{
             request.form.username }}">
         <dt>Password:
         <dd><input type=password name=password>
       </dl>
       <p><input type=submit value=Login>
     </form>
   {% endblock %}

Всплывающие сообщения с категориями
-----------------------------------

.. versionadded:: 0.3

Всплывающему сообщению можно назначить категорию.  По умолчанию, если
категория не указана, используется категория ``'message'`` - сообщение.
Для более качественной обратной связи с пользователем можно указывать
другие категории.  Например, сообщения об ошибках должны отображаться
на красном фоне.

Для вывода всплывающего сообщения с другой категорией, просто передайте
её вторым аргументом функции :func:`~flask.flash`::

    flash(u'Invalid password provided', 'error')

Внутри шаблона можно сообщить функции :func:`~flask.get_flashed_messages`,
что нужно вернуть ещё и категорию.  В этом случае цикл выглядит несколько
иначе:

.. sourcecode:: html+jinja

   {% with messages = get_flashed_messages(with_categories=true) %}
     {% if messages %}
       <ul class=flashes>
       {% for category, message in messages %}
         <li class="{{ category }}">{{ message }}</li>
       {% endfor %}
       </ul>
     {% endif %}
   {% endwith %}

Это просто пример того, как можно отображать всплывающие сообщения.  Можно
использовать категорию для добавления к сообщению префикса, например,
``<strong>Ошибка:</strong>``.

Фильтрация всплывающих сообщений
--------------------------------

.. versionadded:: 0.9

При желании можно передать список категорий, который будет использован
функцией :func:`~flask.get_flashed_messages` для фильтрации сообщений. Это
полезно в тех случаях, если нужно выводить сообщения каждой категории в
отдельном блоке.

.. sourcecode:: html+jinja

    {% with errors = get_flashed_messages(category_filter=["error"]) %}
    {% if errors %}
    <div class="alert-message block-message error">
      <a class="close" href="#">×</a>
      <ul>
        {%- for msg in errors %}
        <li>{{ msg }}</li>
        {% endfor -%}
      </ul>
    </div>
    {% endif %}
    {% endwith %}

Примечания переводчика
``````````````````````

В оригинале были "мигающие" или "вспыхивающие" сообщения, но я счёл
дословный перевод не вполне соответствующим истине - эти сообщения не
мигают и не вспыхивают.  Хотя они также и не всплывают, но это слово
показалось мне более удачным.

`Оригинал этой страницы <http://flask.pocoo.org/docs/patterns/flashing>`_